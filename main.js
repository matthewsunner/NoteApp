
function createNote() {
    var noteTitle = document.getElementById("note-title").value;
    var noteBody = document.getElementById("note-body").value;

    var note = {
        title: noteTitle,
        body: noteBody
    }
    
    if (localStorage.getItem('notes') === null) {
        const notes = [];
        notes.push(note);
        localStorage.setItem('notes', JSON.stringify(notes));
    } else {
        var notes = JSON.parse(localStorage.getItem('notes'));
        notes.push(note);
        localStorage.setItem('notes', JSON.stringify(notes));
    }

    document.getElementById('note-form').reset();
    showNotes();

}

function showNotes() {
    var notes = JSON.parse(localStorage.getItem('notes'));
    var noteOutput = document.getElementById("note-output");
    noteOutput.innerHTML = ``
    for(var i = 0; i < notes.length; i++) {
        var title = notes[i].title;
        var body = notes[i].body;
        noteOutput.innerHTML += '<div class="p-md-1">'+
                                '<div class="card" style="width: 18rem;">'+
                                '<div class="card-body">'+
                                    '<h5 id="test" class="card-title">' +title+ '</h5>'+
                                    '<p class="card-text">' +body+ '</p>'+
                                    '<a onclick="deleteNote(\''+title+'\')" class="btn btn-danger">Delete</a>'+
                                '</div>'+
                                '</div>';
    }
}


function deleteNote(title) {
    var notes = JSON.parse(localStorage.getItem('notes'));
    for (var i = 0; i < notes.length; i++) {
        if (notes[i].title == title) {
            notes.splice(i, 1);
        }
    }
    localStorage.setItem('notes', JSON.stringify(notes));
    showNotes();
}
 
function recentNotes() {
    var notes = JSON.parse(localStorage.getItem('notes')).reverse();
    var noteOutput = document.getElementById("recent-notes");
    for (var i = 0; i < 3; i++) {
        var title = notes[i].title;
        var body = notes[i].body;
        noteOutput.innerHTML += '<div class="p-md-1">'+
                                '<div class="card" style="width: 18rem;">' +
                                '<div class="card-body">' +
                                    '<h5 id="test" class="card-title">' + title + '</h5>' +
                                    '<p class="card-text">' + body + '</p>' +
                                    '<a onclick="deleteNote(\'' + title + '\')" class="btn btn-danger">Delete</a>' +
                                '</div>'+
                                '</div>';
    }
}
