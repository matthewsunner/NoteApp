# NoteApp

This application is a simple plain text note taking application built using Javascript and HTML. The application uses HTML5's local browser storage for persistent note saving abilities. The application can be run from local host by running the index.html page. If you have any questions about using the application, please let me know. 

# Future Features
- Note Editing Functionality
- Note Tagging
- Note Searching
- Note Filtering


